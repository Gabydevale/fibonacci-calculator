import { Router, Request, Response } from "express";

const router = Router();

router.get('/api/fibonacci', (req: Request, res: Response) => {
    res.send(`
        <form method="POST">
        <h1>Calculate Fibonacci</h1>
            <div>
                <input name="number" />
            </div>
            <button>Calculate</button>
        </form>
    `)
});

router.get('/api/fibonacci/:number', (req: Request, res: Response) =>{

    const number2: number = +(req.params.number);

})

router.post('/api/fibonacci', (req: Request, res: Response) => {
    
    const number = req.body.number;

    const fibo = (number:  number) => {      
        let a=0, b=1, result;
        for(let i=2; i<= number; i++){
            result = a + b;
            a=b;
            b= result;
        }
        return result;
    }
    
    if(number == 0 ){
        res.send(`Fibonacci result for ${number} is: 0`)
    }
    else if(number == 1){
        res.send(`Fibonacci result for ${number} is: 1`)
    }else{
        const fibo2 = fibo(number);
        res.send(`Fibonacci result for ${number} is: ${fibo2}`);
    }
});

export { router };