"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = require("express");
const router = (0, express_1.Router)();
exports.router = router;
router.get('/api/fibonacci', (req, res) => {
    res.send(`
        <form method="POST">
        <h1>Calculate Fibonacci</h1>
            <div>
                <input name="number" />
            </div>
            <button>Calculate</button>
        </form>
    `);
});
router.get('/api/fibonacci/:number', (req, res) => {
    const number2 = +(req.params.number);
});
router.post('/api/fibonacci', (req, res) => {
    const number = req.body.number;
    const fibo = (number) => {
        let a = 0, b = 1, result;
        for (let i = 2; i <= number; i++) {
            result = a + b;
            a = b;
            b = result;
        }
        return result;
    };
    if (number == 0) {
        res.send(`Fibonacci result for ${number} is: 0`);
    }
    else if (number == 1) {
        res.send(`Fibonacci result for ${number} is: 1`);
    }
    else {
        const fibo2 = fibo(number);
        res.send(`Fibonacci result for ${number} is: ${fibo2}`);
    }
});
